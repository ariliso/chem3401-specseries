\title{
Determination of a spectrochemical series for \ox{3,Chromium} ligands by evaluation 
of ligand field strength through UV-Vis absorption spectroscopy.}
\author{Ariel Lisogorsky \\ 3061877}
\date{CHEM - 3401 L \\ March 13, 2018}

\documentclass[12pt,titlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=0.85in]{geometry}
\usepackage{indentfirst}
\usepackage{chemfig}
\usepackage{chemmacros}
\usepackage{caption}
\usepackage{achemso}
\usepackage{microtype}
\usepackage{amsmath}
\usepackage[section]{placeins}
\usepackage{varioref}
\usepackage[capitalise,noabbrev]{cleveref}
\usepackage{booktabs}

\setkeys{acs}{articletitle = true}
\usechemmodule{units,redox,reactions}
\chemsetup{redox/pos=side}

\renewcommand*{\thefootnote}{\alph{footnote}} %letters instead of numbers for footnotes

\sisetup{
  round-half      = even,
  table-auto-round,
  multi-part-units= single,
  list-units      = single,
  range-units     = single
}

\captionsetup[figure]{width=0.8\linewidth}
\captionsetup[table]{width=0.8\linewidth}

\begin{document}
\maketitle{}
\begin{abstract}
The ligand field splitting parameter($\Delta_o$) in various \ox{3,Cr} compunds was determined from peak
absorption wavelengths in the 350-750 nm range. A spectrochemical series of $\ch{Cl-}<\ch{C2O4^2-}<\ch{NCS-}<\ch{OH2}<\ch{(en)}$ was observed. \iupac{potassium \ox{3,trioxalochromate}} and \ch{K3[Cr(CNS)6] * 4 H2O}
were synthesised and produced yields of \\TODO{insert yield here when calculated} respectively.
\end{abstract}

\section{Introduction}

When d- block metals form coordination complexes, interactions between the valence
d orbitals in the metal and surrounding ligands affect the energy levels and behaviour of the orbitals. 
In an octahedral complex, crystal field theory describes splitting of d orbital energies in to two levels,
$t2_g$ and $e_g$ depending on their symmetry with respect to the ligands. The $d_{xy}$ , $d_{xz}$ and $d_{yz}$ 
orbitals with $t2_g$ symmetry lower in energy while the $d_{z^2}$ and the $d_{x^2-y^2}$ form the higher $e_g$ energy
level. The difference of these energy levels in an octahedral conformation is referred to as the ligand 
field splitting energy ($\Delta_o$). If there are no additional effects distorting the complex, $t2_g$ orbitals
are by $0.$ \cite{Housecroft2012Inorganic,Buffie2018Inorganic,Shriver2014Inorganic}


Different ligands have differing effects on the strength of $\Delta_o$. The effect to which the energy levels
are affected depends on the bonding characteristics of the ligands and the metals. The strength of $\sigma$ and
$\pi$ bonding and the $\pi$ accepting or donating properties of the bonds between the metal and coordinated ligands
affect the degree to which the energy levels are split. Often, the bonding property with the strongest effect on
splitting characteristics is the tendency to accept a $\pi$ electron pair while donating electrons to form a
 $\sigma$ bond with the metal. Overall, the trend for ligands in order of decreasing strength of field splitting is
 $\sigma$ donors that are $\pi$ acceptors, $\sigma$ and $\pi$ donors, strong $\sigma$ donors and finally, poor
 $\sigma$ donors with the lowest field splitting strength.\cite{Shriver2014Inorganic}

 In complexes of $d^3$ oxidation states like \ox{3,Chromium} the ground state consists of 3 unpaired
 electrons in each of the $t_{2g}$ orbitals. Since the excitation of a $t_{2g}$ electron
 to an $e_g$ anti-bonding orbital is equivalent to $\Delta_o$ ligand field splitting energy in these 
 complexes can - in optimal conditions - be reasonably straightforward to determine from an
 absorption spectrum in the UV-Vis region. \cite{Buffie2018Inorganic}


\section{Materials and Method}

	\begin{table}[h]
		\centering
		\caption{Materials used in experiment and sources. Note: U of W denotes stocks 
			prepared/provided by the university of Winnipeg}
		\label{tbl:mat}
			\begin{tabular}{llcl} \toprule
			Name / Formula & CAS No. & Supplier & Appearance \\ \midrule
			oxalic acid & 144-62-7 & BDH & White granular solid \\
			potassium oxalate & 6487-48-5 & Mallinckrodt & Crystalline white solid \\
			potassium dichromate  & 777-50-9 & U of W & Bright orange Crystals \\
			\ch{KCr(SO4)2 * 12 H2O} & 7788-99-0 & U of W & Dark violet granular solid \\
			\ch{KSCN} & 333-20-0 & U of W & White granular solid \\
			\ox{3,chromium} nitrate nonahydrate & 778902-8 & Fisher & Crystalline dark violet solid  \\
			tris(ethylenediamine)\ox{3,chromium} chloride & 16165-32-5 & Alfa-Aesar & Brown-yellow solid \\
			\iupac{\trans-\ch{[Cr(OH2)4Cl2] * 2 H2O}} & 10060-12-15 & Fischer & Dark violet solid \\
			nitric Acid (0.1M) & & U of W & Transparent solution \\
			hydrochloric acid (0.1M) & & U of W & Transparent solution \\
			\bottomrule	
			\end{tabular}
	\end{table}

\subsection{Synthesis of \ch{K3[Cr(CNS)6] * 4 H2O}}

A solution was prepared by adding \SI{0.7414}{\g} of potassium thiocyanate (\ch{KSCN}) and \SI{0.6487}{\g} of
chrome alum dodecahydrate (\ch{KCr(SO4)2 * 12 H2O}) \SI{5.0}{\mL} of deionized (DI) water. The solids
partially dissolved to form a brown/green solution with dark solids that did not readily dissolve.
The solution was heated above a boiling water bath where eventual dissolution occurred. A blue
tint developed in the green solution as it was heated. As evaporation progressed a dark violet
solid began to crystallize. The solution and crystallized product were heated until the solid
formed was dry. Addition of \SI{95}{\percent} ethanol to the solid resulted in formation of a bright
violet solution. A pale green precipitate was also observed. The ethanol solution was vaccum filtered
to separate the violet ethanol solution from additional pale mint green solid. After an additional wash
of the solid with \SI{20}{\mL} of \SI{95}{\percent} ethanol, the filtrate was collected and evaporated
over a hot water bath. The lilac solid was collected, weighed and stored in an airtight vial for future
analysis.

\subsection{Synthesis of \iupac{potassium \ox{3,trioxalochromate}}}

A clear solution was prepared by adding \SI{3.031}{\g} of oxalic acid to \SI{6.5}{\mL} of DI water.
After full dissolution, \SI{1.0136}{\g} of potassium dichromate was slowly added to the solution.
On contact with the solution, potassium dichromate caused browning of the liquid and vigorous 
evolution of clear gas which began to cause formation of white wisps on contact with room air.
Addition of potassium dichromate was increased as effervescence became less intense and eventually
stopped occurring after addition of additional potassium dichromate. As the resulting dark brownish solution
was brought to a boil its underlying hue changed from brown to violet but the solution remained vary dark. A
\SI{2.317}{\g} portion of potassium oxalate was added to the hot solution and no visible changes occurred. The solution
was removed from heat and an approximately \SI{1.7}{\mL} aliquot of ethanol was added to the solution once it
reached ambient temperature. The mixture was then further cooled on an ice bath.
As the solution cooled, the underlying hue transitioned from violet to blue-green before dark green began to
crystallize out of solution. After crystallization appeared to stop, the mixture was filtered to collect the
dark green crystalline product formed. The pale brown/green filtrate collected was discarded.Two 
additional washings with \SI{6}{\mL} aliquots of \SI{50}{\percent} and a final washing with \SI{16}{\mL}
of ethanol were performed on the remaining solid. The solid was left on the vaccum to dry before collection.
The resulting dark green product was weighed and stored.

\subsection{IR Measurement}

A \SI{50.00}{\mL} solution was prepared by dissolving \SI{0.5085}{\g} \ox{3,chromium} nitrate nonahydrate
in \SI{0.1}{\Molar} nitric acid to yield a. Another  \SI{50.00}{\mL} \SI{7e-3}{\Molar} solution was prepared
by dissolving \ch{[Cr(en)3]Cl3 * 3.5 H2O} in \SI{0.1}{\Molar} hydrochloric acid. Three more \SI{50.00}{\mL} 
solutions with \SIlist{2.5e-2;1e-2;4e-3}{\Molar} concentrations of \iupac{\trans-\ch{[Cr(OH2)4Cl2] * 2 H2O}},
\ch{K3[Cr(ox)3] * 3H2O} and \ch{K3[Cr(CNS)6] * 4H2O} respectively in DI water. Spectra of all 5 solutions
were taken in a polycarbonate cuvette with \SI{1}{\cm} path length in a dual beam UV-Visible spectrophotometer
using DI water as a blank. Wavelengths of absorbance peaks in the spectra of the complexes were recorded (\Cref{tbl:lambda}).
The remaining stock of \iupac{\trans-|\ch{[Cr(OH2)4Cl2] * 2 H2O}} was left in the dark for 1 week and re-analysed
(shown as iii.b in \Cref{tbl:lambda}). After 1 week, the formerly green solution became dull blue. (\Cref{tbl:lambda})

\section{Observations and Results}

Solutions of \ch{[Cr(OH2)6](NO3)3 * 3 H2O}, \ch{[Cr(en)3]Cl3 * 3.5 H2O}, \iupac{\trans-\ch{[Cr(OH2)4Cl2] * 2 H2O}},
\ch{K3[Cr(ox)3] * 3H2O} and \ch{K3[Cr(CNS)6] * 4H2O} were found to be royal blue, brown-yellow, bright green,
blue-green and dark violet. A solution of \iupac{\trans-\ch{[Cr(OH2)4Cl2] * 2 H2O}} changed from bright green to 
dull blue after one week of storage. All solutions produced except \ch{[Cr(en)3]Cl3 * 3.5 H2O} had two
significant absorption peaks in the \SIrange{350}{750}{\nm} region collected however, curvature at the
edge of the region collected suggested that a second absorption peak may have been present 
between \SIlist{350;300}{\nm} (\Cref{tbl:lambda,fig:spectra1})

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.95\textwidth]{3401Spectra.eps}
	\caption{Absorption spectra of fresh solutions of \ch{[Cr(OH2)6](NO3)3 * 3 H2O}, 
		\ch{[Cr(en)3]Cl3 * 3.5 H2O}, \iupac{\trans-\ch{[Cr(OH2)4Cl2] * 2 H2O}},
		\ch{K3[Cr(ox)3] * 3H2O} and \ch{K3[Cr(CNS)6] * 4H2O} depicted as yellow,
		green, black, red and cyan respectively.
	}
	\label{fig:spectra1}
\end{figure}

After a week of storage, absorbance peaks of the solution prepared with \iupac{\trans-\ch{[Cr(OH2)4Cl2] * 2 H2O}}
changed to \SIlist{408;578}{\nm} and its colour changed from green to dark blue.

\begin{table}[htb]
	\centering
	\caption{Peak UV-Vis absorption wavelengths ($\lambda_{max}$) values for aqueous 
	solutions of \ox{3,Chromium} complexes. Results for solutions marked iii.a) and 
	iii.b) taken from same stock immediately after preparation (a) and after 1 week of
	storage(b)} 
	\label{tbl:lambda}
	\begin{tabular}{rlccc}
	\toprule
	&Complex & Colour & $\lambda_{max}$ 1 (nm)& $\lambda_{max} $ 2 (nm) \\
	\midrule
	i)&\ch{[Cr(OH2)6](NO3)3 * 3 H2O} & Royal Blue & 407 & 576 \\
	ii)&\ch{[Cr(en)3]Cl3 * 3.5 H2O} & Brown-Yellow & 452 & \\
	iii.a)&\iupac{\trans-\ch{[Cr(OH2)4Cl2] * 2 H2O}} (New) & Bright Green & 443 & 635 \\
	iii.b)&\iupac{\trans-\ch{[Cr(OH2)4Cl2] * 2 H2O}} (Old) & Muted Blue & 408 & 578 \\
	B)&\ch{K3[Cr(ox)3] * 3H2O} & Blue-Green & 420 & 570 \\
	A)&\ch{K3[Cr(CNS)6] * 4H2O} & Violet & 419 & 565 \\
	\bottomrule
	\end{tabular}
\end{table}

\section{Discussion}
According to literature\cite{Dunne1967Spectra,Shriver2014Inorganic} the spectrochemical series (of the ligands used) for \ox{3,chromium} should be\footnote{bonding ligand written first for ambidentate ligands}:
$\ch{Cl-}<\ch{SCN-}<\ch{C2O4^2-}<\ch{OH2}<\ch{NCS-}<(en)$
fitting the trends discussed in the introduction with regard to the effects
of $\pi$ and $\sigma$ bonding characteristics $\Delta_o$. Though only the simplest case of splitting possible in
octahedral complexes was presented in the introduction, the presence of multiple absorption peaks may suggest that
energy levels of ground or excited states are stabilized by further splitting as a result distortions 
of the octahedral ligand arrangement.\cite{Dunne1967Spectra,Shriver2014Inorganic}

Though literature suggests that $\Delta_o$ is best calculated from the energy represented by the lower of two
absorption bands, the single absorption band observed for the (en) ligand is much closer to the higher absorption
band of the other complexes and spectrochemical series for both sets of bands were calculated by qualitatively
ordering ligands in order of decreasing absorption wavelength. The low energy band suggests a spectrochemical
series of: $\ch{Cl-}<\ch{OH2}<\ch{C2O4^2-}<\ch{NCS-}<<\ch{(en)}$. The high energy absorption band suggests an
order of$\ch{(en)}<\ch{Cl-}<\ch{C2O4^2-}<\ch{NCS-}<\ch{OH2}$. If there is a higher absorption wavelength
present out of the range scanned in the (en) complex at around \SI{350}{\nm} then the resulting series is:
$\ch{Cl-}<\ch{C2O4^2-}<\ch{NCS-}<\ch{OH2}<\ch{(en)}$ which more closely matches predictions.

It according to its position in the spectrochemical series observed, the shorter wavelength absorption
observed in the\ch{NCS-} ligand suggests that nitrogen end of the ligand is coordinating to chromium
instead of sulfur. Were sulfur coordinating, $\Delta_o$ for the complex would be expected to have longer
wavelengths for its absorption peaks than all but the chlorine complex. To confirm this, an IR absorption
spectrum of the compound could be taken to check for absorption resulting from \ch{Cr-S} or \ch{Cr-N} 
vibrational modes in the compound.\cite{Harris2010Quantitative,Shriver2014Inorganic}

It is likely that the complex \iupac{\trans-\ch{[Cr(OH2)4Cl2] * 2 H2O}} was unstable and underwent aquation
over the week it was stored with the hexaaqua ligand having a higher formation constant than the chloride ligand
(and therefore greater stability when both ligands are present). This is supported by the strong similarity
between the peak absorption wavelengths observed of the compound that was left for a week and the hexaaqua
compound prepared(iii.b and i in \Cref{tbl:lambda}).

In conclusion the spectrochemical series prduced qualitatively differed slightly from literature values but
overall conveyed similar trends. 


\section{Appendix}

	\subsection{Reactions}

\Cref{rct:scn} shows the formation of a water soluble 6-coordinate thiocyanate complex  of \ox{3,chromium}. 
\Cref{rct:oxredox,rct:oxprecip} show the redox reaction and subsequent precipitation 
of potassium \ox{3,trioxalatochromate}

	\begin{reactions}
		6 KSCN \aq{} +  KCr(SO4)2 \aq{} &-> K3[Cr(CNS)6] \aq{} + 2 K2SO4 \sld{}  \label{rct:scn} \\
 		Cr2O7^{2-} \aq{} + 8 H+ \aq{} + 3 H2C2O4 \aq{} &->
			2 Cr^{3+} \aq{} + 7 H2O \lqd{} + 6 CO2 \gas{} \label{rct:oxredox} \\
		Cr^{3+} \aq{} + 3 K2C2O4 \aq{} &-> K3[Cr(C2O4)3] \sld{}  + 3 K+ \aq{}  \label{rct:oxprecip}
	\end{reactions}

	\subsection{sample calculations}
	\begin{align}
	\intertext{Concentration of a solution (\ox{3,chromium} nitrate nonahydrate used as example) }
	C &= \frac{\SI{0.5085}{\g}}{\SI{400.14}{\g\per\mol}} \div \SI{50.00e-3}{\L} \\
	&=\SI{2.54e-2}{\Molar}
	\intertext{Energy of a transition from absorption peak}
	E&=\frac{hc}{\lambda} \\
	&=\frac{(\SI{6.626e-34}{\J\second})(\SI{3.00e8}{\m\per\second})}{\SI{452e-9}{\m}} \\
	&=\SI{4.395e-19}{\J} = \SI{2.743}{\eV}
	\end{align}	

\bibliography{Remote}

\end{document}